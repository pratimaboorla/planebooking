package org.flight.booking.models;

public class FlightClassDetails {

    private int no_of_seats;
    private FlightClass fclass;
    private float baseprice;
    private int remainingSeats;


    public FlightClassDetails(int no_of_seats, FlightClass fclass, float baseprice){
        this.no_of_seats = no_of_seats;
        this.fclass = fclass;
        this.baseprice = baseprice;
    }

    public FlightClassDetails(int no_of_seats, FlightClass fclass, float baseprice, int remainingSeats){
        this.no_of_seats = no_of_seats;
        this.fclass = fclass;
        this.baseprice = baseprice;
        this.remainingSeats = remainingSeats;
    }

    public int getRemainingSeats() {
        return remainingSeats;
    }

    public void setRemainingSeats(int remainingSeats) {
        this.remainingSeats = remainingSeats;
    }

    public int getNo_of_seats() {
        return no_of_seats;
    }

    public void setNo_of_seats(int no_of_seats) {
        this.no_of_seats = no_of_seats;
    }

    public FlightClass getFclass() {
        return fclass;
    }

    public void setFclass(FlightClass fclass) {
        this.fclass = fclass;
    }

    public float getBaseprice() {
        return baseprice;
    }

    public void setBaseprice(float baseprice) {
        this.baseprice = baseprice;
    }
}

package org.flight.booking.models;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class FlightScheduleInfo {
    private final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");


    private int flightID;
//    private String flightname;
//    private int baseprice;
    private String htmlfclass;
    private String source;
    private String destination;
    private int no_of_passengers;
    private String dDate;
    private Date departureDate;
    private float totalFare;
    private List<FlightClassDetails> flightClassDetailsList;

    private FlightInfo flightInfo;


    /* public Flight(String flightname, int baseprice,String fclass,String source, String destination,int no_of_passengers, String dDate){
        this.flightname = flightname;
        this.baseprice = baseprice;
        this.htmlfclass = fclass;
//        this.fclass = FlightClass.valueOf(fclass);
        this.source = source;
        this.destination = destination;
        this.no_of_passengers = no_of_passengers;
        this.dDate = dDate;
        try {
            departureDate = sdf.parse(dDate);
        }
        catch(ParseException pe){
            System.out.println("Exception caused while parsing given date");
            pe.printStackTrace();
        }
    }*/

    public FlightScheduleInfo(String source, String destination, String dDate, List<FlightClassDetails> fclass_bp_seats)
    {
//        this.flightname = flightname;
//        this.baseprice = baseprice;
//        this.fclass = fclass;
        this.source = source;
        this.destination = destination;
//        this.no_of_passengers = no_of_passengers;
        this.dDate = dDate;
        this.flightClassDetailsList = fclass_bp_seats;
        try {
            departureDate = sdf.parse(dDate);
        }
        catch(ParseException pe){
            System.out.println("Exception caused while parsing given date");
            pe.printStackTrace();
        }
    }

    public int getFlightID() {
        return flightID;
    }

    public void setFlightID(int flightID)
    {
        this.flightID = flightID;
    }

    public FlightInfo getFlightInfo() {
        return flightInfo;
    }

    public void setFlightInfo(FlightInfo flightInfo) {
        this.flightInfo = flightInfo;
        setFlightID(flightInfo.getFlightID());
    }
//
//    public int getBaseprice() {
//        return baseprice;
//    }
//
//    public void setBaseprice(int baseprice) {
//        this.baseprice = baseprice;
//    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

//    public String getFlightname() {
//        return flightname;
//    }
//
//    public void setFlightname(String flightname) {
//        this.flightname = flightname;
//    }

    public int getNo_of_passengers() {
        return no_of_passengers;
    }

    public void setNo_of_passengers(int no_of_passengers) {
        this.no_of_passengers = no_of_passengers;
    }

    public String getdDate() {
        return dDate;
    }

    public void setdDate(String dDate) {
        this.dDate = dDate;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public List<FlightClassDetails> getFlightClassDetailsList() {
        return flightClassDetailsList;
    }

    public void setFlightClassDetailsList(List<FlightClassDetails> flightClassDetailsList) {
        this.flightClassDetailsList = flightClassDetailsList;
    }

    public String getHtmlfclass() {
        return htmlfclass;
    }

    public void setHtmlfclass(String htmlfclass) {
        this.htmlfclass = htmlfclass;
    }

    public float getTotalFare() {
        return totalFare;
    }

    public void setTotalFare(float totalFare) {
        this.totalFare = totalFare;
    }

}

package org.flight.booking.models;

public class FlightResponse
{
    private int flightID;
    private String flightName;
    private String source;
    private String destination;
    private String deptDate;
    private String flightClass;
    private int no_of_passengers;
    private int totalFare;

    public String getDeptDate() {
        return deptDate;
    }

    public void setDeptDate(String deptDate) {
        this.deptDate = deptDate;
    }

    public int getNo_of_passengers() {
        return no_of_passengers;
    }

    public void setNo_of_passengers(int no_of_passengers) {
        this.no_of_passengers = no_of_passengers;
    }

    public int getFlightID() {
        return flightID;
    }

    public void setFlightID(int flightID) {
        this.flightID = flightID;
    }

    public String getFlightName() {
        return flightName;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getFlightClass() {
        return flightClass;
    }

    public void setFlightClass(String flightClass) {
        this.flightClass = flightClass;
    }

    public int getTotalFare() {
        return totalFare;
    }

    public void setTotalFare(int totalFare) {
        this.totalFare = totalFare;
    }
}

package org.flight.booking.models;

public enum FlightClass {
    FirstClass("FirstClass"),
    BusinessClass("BusinessClass"),
    EconomyClass("EconomyClass");

    private String fclass;

    FlightClass(String fclass){
        this.fclass = fclass;
    }


}

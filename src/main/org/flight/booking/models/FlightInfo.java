package org.flight.booking.models;

public class FlightInfo
{
    private int flightID;
    private String flightName;

    public FlightInfo(int flightID, String flightName){
        this.flightID = flightID;
        this.flightName = flightName;
    }

    public int getFlightID() {
        return flightID;
    }

    public void setFlightID(int flightID) {
        this.flightID = flightID;
    }

    public String getFlightName() {
        return flightName;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }

}

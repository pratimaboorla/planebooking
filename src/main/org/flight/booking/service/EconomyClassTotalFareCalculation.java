package org.flight.booking.service;

import org.flight.booking.models.FlightClass;
import org.flight.booking.models.FlightClassDetails;
import org.flight.booking.models.FlightScheduleInfo;

public class EconomyClassTotalFareCalculation implements TotalFareCalculation
{

    @Override
    public int calculateTotalFare(FlightScheduleInfo f, int seats_to_be_booked, FlightClass selectedFlightClass, String travelDate) {

        FlightClassDetails flight_baseprice_details = f.getFlightClassDetailsList().stream().filter(flightclass -> flightclass.getFclass().equals(FlightClass.EconomyClass)).findFirst().get();

        float baseprice = flight_baseprice_details.getBaseprice();
        int totalSeats = flight_baseprice_details.getNo_of_seats();
        int remainingSeats = flight_baseprice_details.getRemainingSeats();

        float totalFare = 0;
        int first_40percent_of_seats = (int) (totalSeats * 0.4f);
        int next_50percent_of_seats = (int) (totalSeats * 0.5f);
        int last_10percent_of_seats = totalSeats - (first_40percent_of_seats + next_50percent_of_seats);

        if (remainingSeats >= seats_to_be_booked) {
            System.out.println("You can book");

            if(remainingSeats <= last_10percent_of_seats)
            {
                System.out.println("Booking only in 3rd slot");
                totalFare += seats_to_be_booked * (baseprice * 1.6f);
                System.out.println("Total Fare in 3rd slot only  = "+totalFare);
            }
            else if(remainingSeats <= (next_50percent_of_seats + last_10percent_of_seats))
            {
                if(seats_to_be_booked <= next_50percent_of_seats)
                {
                    System.out.println("Booking only in 2nd slot");
                    totalFare += seats_to_be_booked * (baseprice * 1.3f);
                    System.out.println("Total Fare in 2nd slot only  = "+totalFare);
                }
                else
                {
                    System.out.println("Booking done in 2nd and 3rd slot");
                    int seats_to_be_booked_in_2nd_slot = remainingSeats - last_10percent_of_seats;
                    int seats_to_be_booked_in_3rd_slot = seats_to_be_booked - seats_to_be_booked_in_2nd_slot;

                    totalFare += seats_to_be_booked_in_2nd_slot * (baseprice * 1.3f);
                    System.out.println("Total Fare in 2nd slot = "+totalFare);
                    totalFare += seats_to_be_booked_in_3rd_slot * (baseprice * 1.6f);
                    System.out.println("Total Fare in 3rd slot = "+seats_to_be_booked_in_3rd_slot * (baseprice * 1.6f));
                    System.out.println("Final Total fare = "+totalFare);
                }
            }
            else
            {
                int seats_remaining_to_be_booked_in_1st_slot = remainingSeats - (next_50percent_of_seats + last_10percent_of_seats);
                if(seats_to_be_booked <= seats_remaining_to_be_booked_in_1st_slot)
                {
                    System.out.println("Booking only in 1 st slot");
                    totalFare += seats_to_be_booked * baseprice;
                    System.out.println("Total Fare in 1st slot only = "+totalFare);
                }
                else if(seats_to_be_booked <= (seats_remaining_to_be_booked_in_1st_slot + next_50percent_of_seats))
                {
                    System.out.println("Booking in 1st and 2nd slot");
                    int seats_to_be_booked_in_1st_slot = remainingSeats - (next_50percent_of_seats + last_10percent_of_seats);
                    int seats_to_be_booked_in_2nd_slot = seats_to_be_booked - seats_to_be_booked_in_1st_slot;

                    totalFare += seats_to_be_booked_in_1st_slot * baseprice;
                    System.out.println("Total Fare in 1st slot = "+totalFare);
                    totalFare += seats_to_be_booked_in_2nd_slot * (baseprice * 1.3f);
                    System.out.println("Total Fare in 2nd slot = "+seats_to_be_booked_in_2nd_slot * (baseprice * 1.3f));
                    System.out.println("Final total fare  = "+totalFare);
                }
                else
                {
                    System.out.println("Booking in 1st 2nd and 3rd slots");
                    int seats_to_be_booked_in_1st_slot = remainingSeats - (next_50percent_of_seats + last_10percent_of_seats);
                    seats_to_be_booked = seats_to_be_booked - seats_to_be_booked_in_1st_slot;

                    int seats_to_be_booked_in_2nd_slot = next_50percent_of_seats;
                    seats_to_be_booked = seats_to_be_booked - seats_to_be_booked_in_2nd_slot;

                    int seats_to_be_booked_in_3rd_slot = seats_to_be_booked;

                    totalFare += seats_to_be_booked_in_1st_slot * baseprice;
                    System.out.println("Total fare in 1st slot = "+ totalFare);
                    totalFare += seats_to_be_booked_in_2nd_slot * (baseprice * 1.3f);
                    System.out.println("total fare in 2nd slot = "+ seats_to_be_booked_in_2nd_slot * (baseprice * 1.3f));
                    totalFare += seats_to_be_booked_in_3rd_slot * (baseprice * 16.f);
                    System.out.println("Total fare in 3rd slot = "+ seats_to_be_booked_in_3rd_slot * (baseprice * 16.f));
                    System.out.println("Final total fare  = "+totalFare);
                }
            }
        }

        return Math.round(totalFare);
    }
}

package org.flight.booking.service;

import org.flight.booking.models.FlightClass;
import org.flight.booking.models.FlightScheduleInfo;

public interface TotalFareCalculation
{
    int calculateTotalFare(FlightScheduleInfo f, int seats_to_be_booked, FlightClass selectedFlightClass , String travelDate);
}

package org.flight.booking.service;

import org.flight.booking.models.FlightResponse;
import org.flight.booking.models.FlightScheduleInfo;
import org.flight.booking.models.FlightClass;
import org.flight.booking.models.FlightClassDetails;
import org.flight.booking.repository.FlightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class FlightService
{
    @Autowired
    public FlightRepository frepo;
//    private List<Flight> flights = frepo.getAvailableFlights();
    private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");


    public List<FlightResponse> searchflights_with_source_and_destination(String source, String destination, String no_of_passengers, String departureDate, String fclass)
    {
//        frepo.getAllFlights();
        List<FlightScheduleInfo> flights = frepo.getAvailableFlights();

        if(source == null || source .equals(""))
            source = "Hyd";
        if(destination == null || destination.equals(""))
            destination = "Delhi";

        HashMap<String,Integer> totalFareMap = new HashMap<String, Integer>();
        int givenpassengersCount = defaultPassengerCount(no_of_passengers);
        FlightClass selectedFlightClass = defaultFlightClass(fclass);
        Date nextDate = defaultDepartureDate(departureDate);

        List<FlightScheduleInfo> srcdestFlights = new ArrayList<FlightScheduleInfo>();

        for (FlightScheduleInfo f : flights){
            if(f.getSource().equalsIgnoreCase(source) && f.getDestination().equalsIgnoreCase(destination))
            {
                if( f.getDepartureDate().compareTo(nextDate) == 0 ||  f.getDepartureDate().after(nextDate))
                {
                    for(FlightClassDetails fc: f.getFlightClassDetailsList()){

                        if(fc.getFclass() == selectedFlightClass){
                            f.setHtmlfclass(selectedFlightClass.toString());
                            if(fc.getRemainingSeats() >= givenpassengersCount)
                            {

                                int price = calculateTotalFare(f , givenpassengersCount , selectedFlightClass , sdf.format(nextDate));
                                f.setTotalFare(price);
                                if( price > 0)
                                    srcdestFlights.add(f);
                                f.setNo_of_passengers(givenpassengersCount);
                                totalFareMap.put(f.getFlightInfo().getFlightName(), price);
                            }
                        }

                    }

                }
            }

        }
        return srcdestFlights.stream().map(flight -> convertToFlightresponse(flight, selectedFlightClass.toString(), nextDate)).collect(Collectors.toList());

    }


    private FlightResponse convertToFlightresponse(FlightScheduleInfo searchedFlights, String fclass, Date departureDate)
    {
        FlightResponse response = new FlightResponse();

        Instant instant = Instant.ofEpochMilli(departureDate.getTime());
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
        LocalDate localDate = localDateTime.toLocalDate();

        System.out.println("java.util.Date: " + departureDate);
        System.out.println("java.time.LocalDate: " + localDate);

        String date = localDate.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL));
//        System.out.println();



        response.setFlightID(searchedFlights.getFlightID());
        response.setFlightName(searchedFlights.getFlightInfo().getFlightName());
        response.setSource(searchedFlights.getSource());
        response.setDestination(searchedFlights.getDestination());
        response.setFlightClass(fclass);
        response.setTotalFare((int)searchedFlights.getTotalFare());
        response.setNo_of_passengers(searchedFlights.getNo_of_passengers());
        response.setDeptDate(date);

        return response;

    }


    public int defaultPassengerCount(String no_of_passengers){
        int passengers;

        if(no_of_passengers==null || no_of_passengers=="")
            passengers=1;
        else
            passengers=Integer.parseInt(no_of_passengers);

        return passengers;
    }

    public Date defaultDepartureDate(String departureDate){
        Date nextDate = new Date();
        // making default date as next day
        try {
            if(departureDate == null || departureDate == "")
                {
                    Calendar c = Calendar.getInstance();
                    Date today = c.getTime();
                    c.add(Calendar.DAY_OF_YEAR,1);
                    Date d = c.getTime();
                    String strdate = sdf.format(d);
                    nextDate = sdf.parse(strdate);
                    //            System.out.println("Tomorrows date is: "+nextDate);
                }
            else
                {
                    nextDate = sdf.parse(departureDate);
                    //System.out.println("Given Date is : "+nextDate);

                }
            }
            catch(ParseException pe)
            {
                System.out.println(pe.getMessage());
            }

//        System.out.println("Date is : "+sdf.format(nextDate));
     return nextDate;
    }

    public FlightClass defaultFlightClass(String fclass){
        FlightClass flightClass;

        // making default is Economy class
        if(fclass == null || fclass == "")
            flightClass = FlightClass.EconomyClass;
        else
            flightClass = FlightClass.valueOf(fclass);

        return flightClass;
    }


    public int calculateTotalFare(FlightScheduleInfo f, int passengerCount, FlightClass selectedFlightClass , String travelDate){

        int totalFare = Math.round(f.getFlightClassDetailsList().get(0).getBaseprice());

        switch (selectedFlightClass)
        {
            case EconomyClass: totalFare = new EconomyClassTotalFareCalculation().calculateTotalFare(f , passengerCount , selectedFlightClass , travelDate);
                                break;
            case FirstClass: totalFare = new FirstClassTotalFareCalculation().calculateTotalFare(f , passengerCount , selectedFlightClass , travelDate);
                            break;
            case BusinessClass: totalFare = new BusinessClassTotalFareCalculation().calculateTotalFare(f , passengerCount , selectedFlightClass , travelDate);
                            break;
        }


        return totalFare;

    }

   /* public int calculateBusinessClassTotalFare(FlightScheduleInfo f, int passengerCount) {
        System.out.println("Total Fare calculation of Business class ....");

        FlightClassDetails flight_baseprice_details = f.getFlightClassDetailsList().stream().filter(flightclass -> flightclass.getFclass().equals(FlightClass.BusinessClass)).findFirst().get();

        int totalFare = 0;
        float baseprice = flight_baseprice_details.getBaseprice();
        Date departureDate = f.getDepartureDate();

        SimpleDateFormat dayFormatter = new SimpleDateFormat("EEEE");

        String day = dayFormatter.format(departureDate);


        String[] days = {"Sunday" , "Monday" , "Friday"};
           if(Arrays.stream(days).anyMatch(day::equals))
           {
               totalFare += passengerCount * (baseprice * 1.4f);
           }
           else
           {
                totalFare += passengerCount * baseprice;
           }


        return totalFare;
    }

    public int calculateFirstClassTotalFare(FlightScheduleInfo f, int passengerCount , String travelDate) {
        System.out.println("Total Fare calculation of First class ....");

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate givenDate = LocalDate.parse(travelDate, formatter);
        LocalDate deptDate = LocalDate.parse(f.getdDate(), formatter);

        FlightClassDetails flight_baseprice_details = f.getFlightClassDetailsList().stream().filter(flightclass -> flightclass.getFclass().equals(FlightClass.FirstClass)).findFirst().get();

        float baseprice = flight_baseprice_details.getBaseprice();
        int totalFare =0;
        int days = (int) ChronoUnit.DAYS.between(givenDate, deptDate);
        double[] pecentages_on_days_diff = {2 , 1.9 , 1.8 , 1.7 , 1.6 , 1.5 , 1.4 , 1.3 , 1.2 , 1.1 , 1};

        if(days>=0 && days<11)
            totalFare += passengerCount * baseprice * pecentages_on_days_diff[days];

        System.out.println(totalFare);

        return totalFare;

    }

    public int calculateEconomyClassTotalFare(FlightScheduleInfo f, int seats_to_be_booked) {

        FlightClassDetails flight_baseprice_details = f.getFlightClassDetailsList().stream().filter(flightclass -> flightclass.getFclass().equals(FlightClass.EconomyClass)).findFirst().get();

        float baseprice = flight_baseprice_details.getBaseprice();
        int totalSeats = flight_baseprice_details.getNo_of_seats();
        int remainingSeats = flight_baseprice_details.getRemainingSeats();

        float totalFare = 0;
        int first_40percent_of_seats = (int) (totalSeats * 0.4f);
        int next_50percent_of_seats = (int) (totalSeats * 0.5f);
        int last_10percent_of_seats = totalSeats - (first_40percent_of_seats + next_50percent_of_seats);

        if (remainingSeats >= seats_to_be_booked) {
            System.out.println("You can book");

            if(remainingSeats <= last_10percent_of_seats)
            {
                System.out.println("Booking only in 3rd slot");
                totalFare += seats_to_be_booked * (baseprice * 1.6f);
                System.out.println("Total Fare in 3rd slot only  = "+totalFare);
            }
            else if(remainingSeats <= (next_50percent_of_seats + last_10percent_of_seats))
            {
                if(seats_to_be_booked <= next_50percent_of_seats)
                {
                    System.out.println("Booking only in 2nd slot");
                    totalFare += seats_to_be_booked * (baseprice * 1.3f);
                    System.out.println("Total Fare in 2nd slot only  = "+totalFare);
                }
                else
                {
                    System.out.println("Booking done in 2nd and 3rd slot");
                    int seats_to_be_booked_in_2nd_slot = remainingSeats - last_10percent_of_seats;
                    int seats_to_be_booked_in_3rd_slot = seats_to_be_booked - seats_to_be_booked_in_2nd_slot;

                    totalFare += seats_to_be_booked_in_2nd_slot * (baseprice * 1.3f);
                    System.out.println("Total Fare in 2nd slot = "+totalFare);
                    totalFare += seats_to_be_booked_in_3rd_slot * (baseprice * 1.6f);
                    System.out.println("Total Fare in 3rd slot = "+seats_to_be_booked_in_3rd_slot * (baseprice * 1.6f));
                    System.out.println("Final Total fare = "+totalFare);
                }
            }
            else
            {
                int seats_remaining_to_be_booked_in_1st_slot = remainingSeats - (next_50percent_of_seats + last_10percent_of_seats);
                if(seats_to_be_booked <= seats_remaining_to_be_booked_in_1st_slot)
                {
                    System.out.println("Booking only in 1 st slot");
                    totalFare += seats_to_be_booked * baseprice;
                    System.out.println("Total Fare in 1st slot only = "+totalFare);
                }
                else if(seats_to_be_booked <= (seats_remaining_to_be_booked_in_1st_slot + next_50percent_of_seats))
                {
                    System.out.println("Booking in 1st and 2nd slot");
                    int seats_to_be_booked_in_1st_slot = remainingSeats - (next_50percent_of_seats + last_10percent_of_seats);
                    int seats_to_be_booked_in_2nd_slot = seats_to_be_booked - seats_to_be_booked_in_1st_slot;

                    totalFare += seats_to_be_booked_in_1st_slot * baseprice;
                    System.out.println("Total Fare in 1st slot = "+totalFare);
                    totalFare += seats_to_be_booked_in_2nd_slot * (baseprice * 1.3f);
                    System.out.println("Total Fare in 2nd slot = "+seats_to_be_booked_in_2nd_slot * (baseprice * 1.3f));
                    System.out.println("Final total fare  = "+totalFare);
                }
                else
                {
                    System.out.println("Booking in 1st 2nd and 3rd slots");
                    int seats_to_be_booked_in_1st_slot = remainingSeats - (next_50percent_of_seats + last_10percent_of_seats);
                    seats_to_be_booked = seats_to_be_booked - seats_to_be_booked_in_1st_slot;

                    int seats_to_be_booked_in_2nd_slot = next_50percent_of_seats;
                    seats_to_be_booked = seats_to_be_booked - seats_to_be_booked_in_2nd_slot;

                    int seats_to_be_booked_in_3rd_slot = seats_to_be_booked;

                    totalFare += seats_to_be_booked_in_1st_slot * baseprice;
                    System.out.println("Total fare in 1st slot = "+ totalFare);
                    totalFare += seats_to_be_booked_in_2nd_slot * (baseprice * 1.3f);
                    System.out.println("total fare in 2nd slot = "+ seats_to_be_booked_in_2nd_slot * (baseprice * 1.3f));
                    totalFare += seats_to_be_booked_in_3rd_slot * (baseprice * 16.f);
                    System.out.println("Total fare in 3rd slot = "+ seats_to_be_booked_in_3rd_slot * (baseprice * 16.f));
                    System.out.println("Final total fare  = "+totalFare);
                }
            }
        }

        return Math.round(totalFare);
    }*/

}

















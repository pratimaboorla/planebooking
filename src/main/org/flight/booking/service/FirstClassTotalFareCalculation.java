package org.flight.booking.service;

import org.flight.booking.models.FlightClass;
import org.flight.booking.models.FlightClassDetails;
import org.flight.booking.models.FlightScheduleInfo;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class FirstClassTotalFareCalculation implements TotalFareCalculation
{
    @Override
    public int calculateTotalFare(FlightScheduleInfo f, int seats_to_be_booked, FlightClass selectedFlightClass, String travelDate) {


        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate givenDate = LocalDate.parse(travelDate, formatter);
        LocalDate deptDate = LocalDate.parse(f.getdDate(), formatter);

        FlightClassDetails flight_baseprice_details = f.getFlightClassDetailsList().stream().filter(flightclass -> flightclass.getFclass().equals(FlightClass.FirstClass)).findFirst().get();

        float baseprice = flight_baseprice_details.getBaseprice();
        int totalFare =0;
        int days = (int) ChronoUnit.DAYS.between(givenDate, deptDate);
        double[] pecentages_on_days_diff = {2 , 1.9 , 1.8 , 1.7 , 1.6 , 1.5 , 1.4 , 1.3 , 1.2 , 1.1 , 1};

        if(days>=0 && days<11)
            totalFare += seats_to_be_booked * baseprice * pecentages_on_days_diff[days];

        System.out.println(totalFare);

        return totalFare;
    }
}

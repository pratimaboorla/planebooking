package org.flight.booking.service;

import org.flight.booking.models.FlightClass;
import org.flight.booking.models.FlightClassDetails;
import org.flight.booking.models.FlightScheduleInfo;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Date;

public class BusinessClassTotalFareCalculation implements TotalFareCalculation
{
    @Override
    public int calculateTotalFare(FlightScheduleInfo f, int seats_to_be_booked, FlightClass selectedFlightClass, String travelDate) {

        FlightClassDetails flight_baseprice_details = f.getFlightClassDetailsList().stream().filter(flightclass -> flightclass.getFclass().equals(FlightClass.BusinessClass)).findFirst().get();

        int totalFare = 0;
        float baseprice = flight_baseprice_details.getBaseprice();
        Date flightDepartureDate = f.getDepartureDate();

        Instant instant = Instant.ofEpochMilli(flightDepartureDate.getTime());
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
        LocalDate flightlocalDepartureDate = localDateTime.toLocalDate();

//        System.out.println("java.util.Date: " + flightDepartureDate);
//        System.out.println("java.time.LocalDate: " + flightlocalDepartureDate);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate givenDate = LocalDate.parse(travelDate, formatter);

        int diffdays = (int) ChronoUnit.DAYS.between(givenDate, flightlocalDepartureDate);
//        System.out.println(diffdays);

        SimpleDateFormat dayFormatter = new SimpleDateFormat("EEEE");

        String day = dayFormatter.format(flightDepartureDate);

        if(diffdays <= 28)
        {
            String[] days = {"Sunday" , "Monday" , "Friday"};
            if(Arrays.stream(days).anyMatch(day::equals))
            {
                totalFare += seats_to_be_booked * (baseprice * 1.4f);
            }
            else
            {
                totalFare += seats_to_be_booked * baseprice;
            }
        }
        return totalFare;
    }
}

package org.flight.booking.controller;

import org.flight.booking.models.FlightResponse;
import org.flight.booking.models.FlightScheduleInfo;
import org.flight.booking.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@RestController
public class FlightController {

    @Autowired
    public FlightService fs;
    private SimpleDateFormat sdf= new SimpleDateFormat("dd/MM/yyyy");

//    @RequestMapping("/getFlights")
//    public List<Flight> getFlights(){
//
//        return fs.getAllFlights();
//    }
//
//    @RequestMapping("/getFlights/{fname}")
//    public List<Flight> getFlightsByName(@PathVariable String fname){
//        return fs.getFlightsWithName(fname);
//    }

    //    @RequestMapping("/getsrcdestFlights/{source}/{destination}/{no_of_passengers}")
  /*  @RequestMapping("/flights/search")
    @CrossOrigin
    public ResponseEntity<List<FlightScheduleInfo>> getSourceDestinationFlights(@RequestParam String source,
                                                                                @RequestParam String destination ,
                                                                                @RequestParam(required = false) String no_of_passengers,
                                                                                @RequestParam String departureDate,
                                                                                @RequestParam String fclass)
    {
        List<FlightScheduleInfo> flights = new ArrayList<FlightScheduleInfo>();
        flights = fs.searchflights_with_source_and_destination(source, destination, no_of_passengers,departureDate, fclass);

        return ResponseEntity.ok(flights);
    }*/


    @RequestMapping("/flights/search")
    @CrossOrigin
    public ResponseEntity<List<FlightResponse>> getSourceDestinationFlights(@RequestParam String source,
                                                                            @RequestParam String destination ,
                                                                            @RequestParam(required = false) String no_of_passengers,
                                                                            @RequestParam String departureDate,
                                                                            @RequestParam String fclass)
    {
        List<FlightResponse> flights = new ArrayList<FlightResponse>();
        flights = fs.searchflights_with_source_and_destination(source, destination, no_of_passengers,departureDate, fclass);

        return ResponseEntity.ok(flights);
    }

    @RequestMapping("/react-flightSearch")
    @CrossOrigin
    public ResponseEntity<List<FlightScheduleInfo>> getFlightsWithReactJS(@RequestParam String source,
                                                                                @RequestParam String destination ,
                                                                                @RequestParam(required = false) String no_of_passengers,
                                                                                @RequestParam String departureDate,
                                                                                @RequestParam String fclass)
    {
        List<FlightScheduleInfo> flights = new ArrayList<FlightScheduleInfo>();
//        flights = fs.searchflights_with_source_and_destination(source, destination, no_of_passengers,departureDate, fclass);

        return ResponseEntity.ok(flights);
    }
/*
//    @RequestMapping("/getsrcdestFlights/{source}/{destination}")
    @GetMapping("/getsrcdestFlights")
    @ResponseBody
    public List<Flight> getSourceDestinationFlights(@PathVariable String source, @PathVariable String destination){
        return fs.searchflights_with_source_and_destination(source,destination,1);
    }*/

   /* @RequestMapping("/getsrcFlights/{source}")
    public List<Flight> getSourceDestinationFlights(@PathVariable String source) {
        return fs.searchflights_with_source(source);
    }*/

}

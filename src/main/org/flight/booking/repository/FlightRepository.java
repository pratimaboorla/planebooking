package org.flight.booking.repository;

import org.flight.booking.models.FlightScheduleInfo;
import org.flight.booking.models.FlightClass;
import org.flight.booking.models.FlightClassDetails;
import org.flight.booking.models.FlightInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ArrayList;


@Repository
public class FlightRepository{
    private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    private List<FlightScheduleInfo> flights;

    @Autowired
    public JdbcTemplate jdbcTemplate;

    public FlightRepository(){

        // creating first Flight object
        FlightInfo flightInfo1 = new FlightInfo(1,"Boeing 777");
        ArrayList<FlightClassDetails> flightClassDetails1 = new ArrayList<FlightClassDetails>();

        flightClassDetails1.add(new FlightClassDetails(8,FlightClass.FirstClass,20000, 4));
        flightClassDetails1.add(new FlightClassDetails(35,FlightClass.BusinessClass,13000, 30));
        flightClassDetails1.add(new FlightClassDetails(195,FlightClass.EconomyClass,6000, 130));

        FlightScheduleInfo f1 = new FlightScheduleInfo( "Hyd","Delhi","06/09/2019",flightClassDetails1);
        f1.setFlightInfo(flightInfo1);

        ArrayList<FlightClassDetails> flightClassDetails2 = new ArrayList<FlightClassDetails>();
        FlightInfo flightInfo2 = new FlightInfo(2,"Airbus A319 V2");
        flightClassDetails2.add(new FlightClassDetails(144,FlightClass.EconomyClass,4000, 45));
        FlightScheduleInfo f2= new FlightScheduleInfo("Hyd","Delhi","02/09/2019",flightClassDetails2);
        f2.setFlightInfo(flightInfo2);

        ArrayList<FlightClassDetails> flightClassDetails3 = new ArrayList<FlightClassDetails>();
        FlightInfo flightInfo3 = new FlightInfo(3,"Airbus A321");
        flightClassDetails3.add(new FlightClassDetails(20,FlightClass.BusinessClass,10000, 15));
        flightClassDetails3.add(new FlightClassDetails(152,FlightClass.EconomyClass,5000, 100));
        FlightScheduleInfo f3= new FlightScheduleInfo("Hyd","Delhi","20/09/2019",flightClassDetails3);
        f3.setFlightInfo(flightInfo3);

        FlightScheduleInfo f4 = new FlightScheduleInfo( "Delhi","Hyd","05/09/2019",flightClassDetails1);
        f1.setFlightInfo(flightInfo1);

        flights = new ArrayList<FlightScheduleInfo>();
        flights.add(f1);
        flights.add(f2);
        flights.add(f3);
        flights.add(f4);

    }

    public List<FlightScheduleInfo> getAvailableFlights(){
        //setFlights();
        return flights;
    }

    public void getAllFlights(){

        String qry = "select * from flights";
        System.out.println(jdbcTemplate.queryForList(qry));
    }

}

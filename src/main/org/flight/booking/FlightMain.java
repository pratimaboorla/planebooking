package org.flight.booking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FlightMain
{
    public static void main(String[] args) {
        SpringApplication.run(FlightMain.class,args);
    }
}

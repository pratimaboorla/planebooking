$(document).ready(function () {

   $("#flightsTable").hide();
    $("#availFlightsHeading").hide();

    $("#search-form").submit(function (event) {

        //stop submit the form, we will post it manually.
        event.preventDefault();

        fire_ajax_submit();

    });

   /* $("#availableFlights").submit(function (event){

        alert("Booking...");
         var checked = $('input[name=option]:checked', '#flightsTable').val();
              alert(checked);

    });*/

});


function fire_ajax_submit() {

 var tablelength = $("#table tr").length;
       for( var j=1;j<tablelength; j++){
           $("#"+j).remove();
       }

 var search ={}
 search["source"] = $("#source").val();
 search["destination"] = $("#destination").val();
 search["no_of_passengers"] = $("#no_of_passengers").val();
 search["departureDate"] = $("#departureDate").val();
 search["fclass"]  = $("#fclass").val();

 //search["fclass"] = $("#fclass").val();

 $("#btnsearch").prop("disabled", true);

 $.ajax({
    type: "GET",
            contentType: "application/json",
            url: "/flights/search",
            data: {"source": search.source,
            "destination": search.destination,
            "no_of_passengers": search.no_of_passengers,
            "departureDate" : search.departureDate,
            "fclass" : search.fclass},
            dataType: 'json',
            cache: false,
            timeout: 600000,
            success: function (data) {

                        var txt = "";

                        if(data){
                        var len = data.length;

                        if(len>0){

                                txt += "<tbody id='tbodyjs'>";

                            for(var i=0;i<len;i++)
                            {

                                txt += "<tr id ='"+(i+1)+"'>";
//                                txt += "<td><input type='radio' name='option' "+"id='r"+i+"' "+"value ="+data[i]+"/><td>";
                                txt +="<td>"+data[i].flightID+"</td>";
                                txt +="<td>"+data[i].flightName+"</td>";
                                txt +="<td>"+data[i].source+"</td>";
                                txt +="<td>"+data[i].destination+"</td>";
                                txt += "<td>"+data[i].no_of_passengers+"</td>";
                                txt += "<td>"+data[i].deptDate+"</td>";
                                txt += "<td>" + data[i].flightClass+"</td>";
                                txt += "<td>"+data[i].totalFare+"</td></tr>";

                             }
                             txt += "</tbody>"
                         }

                        if(len>0)
                            {
                                $("#table").append(txt).removeClass("hidden");
                                $("#flightsTable").show();
                                 $("#availFlightsHeading").hide();
                            }
                        else{
//                                console.log("reached");
//                                txt += "<tbody><tr><h2>>No Flights match the given criteria...</h2><tr></tbody>";
//                                $("#table").append(txt).removeClass("hidden");
                                $("#availFlightsHeading").removeClass("hidden");
                                 $("#availFlightsHeading").show();
                                $("#flightsTable").hide();

                            }
                        }


//                        var checked = $('input[name=option]:checked', '#flightsTable').val();
//                        alert(checked);


//                        var json = "<h4>Ajax Response</h4><pre>"
//                            + JSON.stringify(data, null, 4) + "</pre>";
//                        $('#feedback').html(json);

                        console.log("SUCCESS : ", data);
                        $("#btnsearch").prop("disabled", false);

                    },
                    error: function (e) {

                        var json = "<h4>Ajax Response</h4><pre>"
                            + e.responseText + "</pre>";
                        $('#feedback').html(json);

                        console.log("ERROR : ", e);
                        $("#btnsearch").prop("disabled", false);

                    }
 });


}
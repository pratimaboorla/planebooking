import org.flight.booking.models.*;
import org.flight.booking.repository.FlightRepository;
import org.flight.booking.service.FlightService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

public class FlightServiceTest {
    private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    @InjectMocks
    private FlightService fs;

    @Mock
    private FlightRepository frepo;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
    }

    private List<FlightScheduleInfo> getTestData()
    {
        // creating first Flight object
        FlightInfo flightInfo1 = new FlightInfo(1,"Boeing 777");
        ArrayList<FlightClassDetails> flightClassDetails1 = new ArrayList<FlightClassDetails>();

        flightClassDetails1.add(new FlightClassDetails(8,FlightClass.FirstClass,20000, 4));
        flightClassDetails1.add(new FlightClassDetails(35,FlightClass.BusinessClass,13000, 30));
        flightClassDetails1.add(new FlightClassDetails(195,FlightClass.EconomyClass,6000, 130));

        FlightScheduleInfo f1 = new FlightScheduleInfo( "Hyd","Delhi","06/09/2019",flightClassDetails1);
        f1.setFlightInfo(flightInfo1);

        ArrayList<FlightClassDetails> flightClassDetails2 = new ArrayList<FlightClassDetails>();
        FlightInfo flightInfo2 = new FlightInfo(2,"Airbus A319 V2");
        flightClassDetails2.add(new FlightClassDetails(144,FlightClass.EconomyClass,4000, 45));
        FlightScheduleInfo f2= new FlightScheduleInfo("Hyd","Delhi","02/09/2019",flightClassDetails2);
        f2.setFlightInfo(flightInfo2);

        ArrayList<FlightClassDetails> flightClassDetails3 = new ArrayList<FlightClassDetails>();
        FlightInfo flightInfo3 = new FlightInfo(3,"Airbus A321");
        flightClassDetails3.add(new FlightClassDetails(20,FlightClass.BusinessClass,10000, 15));
        flightClassDetails3.add(new FlightClassDetails(152,FlightClass.EconomyClass,5000, 100));
        FlightScheduleInfo f3= new FlightScheduleInfo("Hyd","Delhi","20/09/2019",flightClassDetails3);
        f3.setFlightInfo(flightInfo3);

        FlightScheduleInfo f4 = new FlightScheduleInfo( "Delhi","Hyd","05/09/2019",flightClassDetails1);
        f1.setFlightInfo(flightInfo1);



        return Arrays.asList(f1,f2,f3,f4);
    }

    @Test
    public void displayFlightDetailsForSourceisEmpty() {

        List<FlightScheduleInfo> res = getTestData();

        Mockito.when(frepo.getAvailableFlights()).thenReturn(res);


//        List<FlightScheduleInfo> actualFlightList = new ArrayList();

        List<FlightResponse> actualFlightList = new ArrayList();

        actualFlightList = fs.searchflights_with_source_and_destination("Hyd", "Delhi","","", "");

        assertEquals(3, actualFlightList.size());
    }

    @Test
    public void displayFlightDetailsWhenDestinationisEmpty() {

        List<FlightScheduleInfo> res = getTestData();

        Mockito.when(frepo.getAvailableFlights()).thenReturn(res);


//        List<FlightScheduleInfo> actualFlightList = new ArrayList();

        List<FlightResponse> actualFlightList = new ArrayList();

        actualFlightList = fs.searchflights_with_source_and_destination("Hyd", "","","", "");

        assertEquals(3, actualFlightList.size());
    }


    @Test
    public void displayFlightDetailsWhenSourceAndDestinationAreGivenWithPassengerCount() {

        List<FlightScheduleInfo> res = getTestData();

        Mockito.when(frepo.getAvailableFlights()).thenReturn(res);


//        List<FlightScheduleInfo> actualFlightList = new ArrayList();

        List<FlightResponse> actualFlightList = new ArrayList();

        actualFlightList = fs.searchflights_with_source_and_destination("Hyd", "Delhi","2","", "");

        assertEquals(3, actualFlightList.size());
    }

    @Test
    public void displayFlightDetailsForSrcAndDestWithoutPassengers() {

        List<FlightScheduleInfo> res = getTestData();

        Mockito.when(frepo.getAvailableFlights()).thenReturn(res);


//        List<FlightScheduleInfo> actualFlightList = new ArrayList<FlightScheduleInfo>();
        List<FlightResponse> actualFlightList = new ArrayList();


        actualFlightList = fs.searchflights_with_source_and_destination("Hyd", "Delhi", "","","");


        assertEquals(3,actualFlightList.size());

    }

    @Test
    public void displayFlightDetailsWhenSourceDestinationPassesgerCountAndDateAreGiven() {

        List<FlightScheduleInfo> res = getTestData();

        Mockito.when(frepo.getAvailableFlights()).thenReturn(res);

//             List<FlightScheduleInfo> actualFlightList = new ArrayList<FlightScheduleInfo>();
        List<FlightResponse> actualFlightList = new ArrayList();

        actualFlightList = fs.searchflights_with_source_and_destination("Hyd","Delhi","2" ,"29/08/2019","");

        assertEquals(3,actualFlightList.size());
    }

    @Test
    public void displayFlightDetailsForSrcToDestOnGivenDateAndFlightClass(){

        List<FlightScheduleInfo> res = getTestData();

        Mockito.when(frepo.getAvailableFlights()).thenReturn(res);

//        List<FlightScheduleInfo> actualFlightList = new ArrayList<FlightScheduleInfo>();
        List<FlightResponse> actualFlightList = new ArrayList();


        actualFlightList = fs.searchflights_with_source_and_destination("Hyd","Delhi","2" ,"29/08/2019","BusinessClass");

        assertEquals(2, actualFlightList.size());
    }

    @Test
    public void testEconomyClassTotalFare(){

//        Mockito.when(frepo.getAvailableFlights()).thenReturn(getTestData());

        FlightInfo flightInfo1 = new FlightInfo(1,"Boeing 777");
        ArrayList<FlightClassDetails> fclass_bp_seats1 = new ArrayList<FlightClassDetails>();

        fclass_bp_seats1.add(new FlightClassDetails(8,FlightClass.FirstClass,20000, 4));
        fclass_bp_seats1.add(new FlightClassDetails(35,FlightClass.BusinessClass,13000, 30));
        fclass_bp_seats1.add(new FlightClassDetails(195,FlightClass.EconomyClass,6000, 110));

        FlightScheduleInfo f1 = new FlightScheduleInfo( "Hyd","Delhi","25/08/2019",fclass_bp_seats1);
        f1.setFlightInfo(flightInfo1);



        int actualTotalFare = fs.calculateTotalFare(f1,100 , FlightClass.EconomyClass , "25/08/2019");
        assertEquals(798000, actualTotalFare);



    }

    @Test
    public void testBusinessClassTotalFare(){
        FlightInfo flightInfo1 = new FlightInfo(1,"Boeing 777");
        ArrayList<FlightClassDetails> fclass_bp_seats1 = new ArrayList<FlightClassDetails>();

        fclass_bp_seats1.add(new FlightClassDetails(8,FlightClass.FirstClass,20000, 4));
        fclass_bp_seats1.add(new FlightClassDetails(35,FlightClass.BusinessClass,13000, 20));
        fclass_bp_seats1.add(new FlightClassDetails(195,FlightClass.EconomyClass,6000, 130));

        FlightScheduleInfo f1 = new FlightScheduleInfo( "Hyd","Delhi","29/09/2019",fclass_bp_seats1);
        f1.setFlightInfo(flightInfo1);

        int actualTotalFare = fs.calculateTotalFare(f1, 5, FlightClass.BusinessClass , "02/09/2019");
        assertEquals(91000, actualTotalFare);

//        int actualTotalFare = fs.calculateTotalFare(f1, 5, FlightClass.BusinessClass , "29/08/2019");
//        assertEquals(0, actualTotalFare);

    }

    @Test
    public void testFirstClassTotalFare()
    {
        FlightInfo flightInfo1 = new FlightInfo(1,"Boeing 777");
        ArrayList<FlightClassDetails> fclass_bp_seats1 = new ArrayList<FlightClassDetails>();

        fclass_bp_seats1.add(new FlightClassDetails(8,FlightClass.FirstClass,20000, 4));
        fclass_bp_seats1.add(new FlightClassDetails(35,FlightClass.BusinessClass,13000, 20));
        fclass_bp_seats1.add(new FlightClassDetails(195,FlightClass.EconomyClass,6000, 130));

        FlightScheduleInfo f1 = new FlightScheduleInfo( "Hyd","Delhi","31/08/2019",fclass_bp_seats1);
        f1.setFlightInfo(flightInfo1);

        int actualTotalFare = fs.calculateTotalFare(f1, 5 , FlightClass.FirstClass,"25/08/2019");

        assertEquals(140000, actualTotalFare);

    }

}
